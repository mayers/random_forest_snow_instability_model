# Random forest model for the assessment of snow instability

This repository contains the random forest (RF) model developed in the publication:

Mayer, S., van Herwijnen, A., Techel, F., and Schweizer, J.: A random forest model to assess snow instability from simulated snow stratigraphy, The Cryosphere Discussions, [https://doi.org/10.5194/tc-2022-34](https://doi.org/10.5194/tc-2022-34), 2022

Given a snow profile simulated with the numerical snow cover model [SNOWPACK](https://code.wsl.ch/snow-models/snowpack), the RF model provides a probability of instability for every layer of the profile. The required input features are computed from the basic SNOWPACK output. 


## How to use the RF model

Please have a look at the [Examples](scripts/Examples.ipynb) which illustrate how to use the RF model. 
For any question or issue, please contact Stephanie via email ([stephanie.mayer@slf.ch](stephanie.mayer@slf.ch)) or just open an issue in the main repository.

## License
[MIT](https://choosealicense.com/licenses/mit/)

