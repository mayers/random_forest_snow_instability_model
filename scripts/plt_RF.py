# -*- coding: utf-8 -*-
"""
Created on Wed Apr 14 08:40:05 2021

@author: Stephanie Mayer
"""


import numpy as np
import pandas as pd
import datetime
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import matplotlib.dates as mdates
from scipy import stats as stats

#%% settings for plots
       
SMALL_SIZE = 12
MEDIUM_SIZE = 16
BIGGER_SIZE = 20

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=SMALL_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title



    ###########################################################################################################
#####
#####   Name:       plot_sp_single_P0
#####
#####   Purpose:     given a single snow profile and the P_unstable values for each layer, plot profile (gt, hardness) & P_unstable
#####               
#####   Remarks: 
#####               
###########################################################################################################

def plot_sp_single_P0(fig, ax, prof, var, colorbar=True):
    cmap = ['greenyellow', 'darkgreen', 'pink', 'lightblue', 'blue', 'magenta', 'red', 'cyan', 'lightblue']
    hh = prof['hardness']
    layer_top = prof['layer_top']
    P_unstable = prof[var]
    nr_uppermost = len(layer_top) - 1
    gt = divmod(prof['graintype'],100)[0].astype(int)

    ########### contours ##########################
    ax.plot([0, 0], [0, layer_top.iloc[-1]], c='black', linewidth=1)
    ax.plot([0, -hh[0]], [0, 0], c='black', linewidth=1)

    ################ Plot profile against hand hardness ########################
    for iy, y in enumerate(layer_top):
        if iy == 0: y1 = 0

        if iy == nr_uppermost:
            ax.plot([0, -hh[iy]], [y, y], c='black', linewidth=1)
        else:
            ax.plot([0, np.max([-hh[iy], -hh[iy + 1]])], [y, y], c='black', linewidth=1)
        ax.plot([-hh[iy], -hh[iy]], [y1, y], c='black', linewidth=1)

        ax.fill_betweenx([y1, y], 0, -hh[iy], color=cmap[int(gt[iy]) - 1])
        y1 = y


    ax.set_xlim(0, 5.5)
    ax.set_xticks(np.arange(0, 5.5, 1))
    ax.set_xticklabels(['', 'F ', '4F', '1F', 'P', 'K'])
    ax.set_ylabel('Snow depth [cm]')
    ax.set_xlabel('hand hardness')

    #  ######### Plot P_unstable at 2nd x axis ########################
    if len(P_unstable) > 0:
        ax11 = ax.twiny()
        var = P_unstable
        height_var= np.repeat(np.concatenate((np.array([0]), layer_top)), 2)[1:-1]
        var_repeat = np.repeat(var, 2)
        ax11.plot(var_repeat, height_var, c='black', linewidth=2.5, label='$\mathregular{P_{unstable}}$')

        ax11.set_xlim([0, 1])
        ax11.set_xticks([0, 0.5, 1])
        ax11.set_xticklabels(['0', '0.5', '1'])
        ax11.set_xlabel('$P_{unstable}$')
        ax11.legend(loc=1)
    ################ colorbar ########################################################
    if colorbar:
        ax_pos = np.array(ax.get_position())
        axcolor = fig.add_axes([0.85, ax_pos[0, 1], 0.02, ax_pos[1, 1] - ax_pos[0, 1]])
        #       axcolor.set_title('grain type')

        cmapcolorbar = ['greenyellow', 'darkgreen', 'pink', 'lightblue', 'blue', 'magenta', 'red', 'cyan']
        ticklabels = ['PP', 'DF', 'RG', 'FC', 'DH', 'SH', 'MF', 'IF']
        cmapc = mpl.colors.ListedColormap(cmapcolorbar)
        bounds = np.arange(len(cmapcolorbar) + 1)

        norm = mpl.colors.BoundaryNorm(bounds, cmapc.N)
        ticks = bounds + .5

        cb1 = mpl.colorbar.ColorbarBase(axcolor, cmap=cmapc, norm=norm, ticks=ticks, orientation='vertical', label = 'grain type')
        cb1.ax.set_yticklabels(ticklabels)
        plt.subplots_adjust(bottom=0.1, right=0.8, top=0.9)
        
        
    return ax11  
        


##########################################################################################################
####
####   Name: plot_evo_SP
####
####   Purpose: plot SP simulation, evolution of layers (in terms of variable "var") over time 
#####   
####    Remarks:
###########################################################################################################

def plot_evo_SP(df_evo, fig, ax, start, stop, var, colorbar = True, resolution = 'D'):
    

    cgt=['greenyellow','darkgreen','pink','lightblue','blue','magenta','red','cyan','lightblue']   
    cmap=mcolors.ListedColormap(cgt)

#%% get prof data
    mydates = pd.date_range(start, stop, freq = resolution).tolist()

#   for tt in ts:
    for ts in mydates:

        df = df_evo[df_evo['datetime'] == ts]
        df.reset_index(inplace = True, drop = True)
       

        depth = np.array(df['layer_top'])
        depth_edges = np.concatenate((np.array([0]),depth))
   
        gt = divmod(df['graintype'],100)[0].astype(int)
    

#       crust = [int(str(gt_code)[-1]) for gt_code in prof['grain_type']]
       
        if len(gt)==0: continue

        x=[ts,ts+datetime.timedelta(days=1)]
        if var == 'graintype':
            cb=ax.pcolormesh(x,depth_edges,np.array([gt,gt]).transpose(),cmap=cmap,vmin=0.5,vmax=9.5)#,alpha=0.9)
        else:
            p=df[var]      
            cmap = plt.cm.viridis  # define the colormap
            cmaplist = [cmap(i) for i in range(cmap.N)]
            cmap = mpl.colors.LinearSegmentedColormap.from_list('Custom cmap', cmaplist, cmap.N)

            # define the bins and normalize
            bounds = np.linspace(0, 1, 11)
            norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
            cb=ax.pcolormesh(x,depth_edges,np.array([p,p]).transpose(), cmap = cmap, norm = norm) # cmap='PuBu_r'

    ax.set_ylabel('Snow depth [cm]')
    ax.set_xlabel('Date')
    ax.set_xlim(start,stop)
    # rotate and align the tick labels so they look better
    fig.autofmt_xdate()
    
    # use a more precise date string
    ax.fmt_xdata = mdates.DateFormatter('%Y-%m-%d')    

    
    if colorbar == True:
        ax_pos = np.array(ax.get_position())
        axcolor = fig.add_axes([0.9, ax_pos[0,1], 0.02, ax_pos[1,1] - ax_pos[0,1]])
    
        if var == 'graintype':
            cgt_no_rf = ['greenyellow', 'darkgreen', 'pink', 'lightblue', 'blue', 'magenta', 'red', 'cyan']
            cmapc = mcolors.ListedColormap(np.array(cgt_no_rf))
            ticks = np.arange(9)
            norm = mpl.colors.BoundaryNorm(ticks + 0.5, cmapc.N)
            cbar = mpl.colorbar.ColorbarBase(axcolor, cmap=cmapc,
                           norm=norm,
                           ticks=ticks,
                           orientation='vertical')
            cbar.set_ticklabels(['PP','DF','RG','FC','DH','SH','MF(cr)','IF'])
            cbar.set_label('Grain type')
        else:
            cbar=plt.colorbar(cb,axcolor)
            cbar.set_label('$P_\mathrm{unstable}$')
   
    #plt.subplots_adjust(right=0.9)
    fig.autofmt_xdate()

       
    return cb



